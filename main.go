package main

import (
	"errors"
	"flag"
	"fmt"
	"net/http"
	"net/url"

	"os"

	"time"

	"github.com/kurrik/oauth1a"
	"github.com/kurrik/twittergo"
)

type (
	credentials struct {
		consumerKey       string
		consumerSecret    string
		accessToken       string
		accessTokenSecret string
	}

	result struct {
		n     int
		count int
	}
)

var usage = `Usage: swit [options...]

Options:
  -at  Twitter Access Token.
  -as  Twitter Access Token Secret.
  -ck  Twitter Consumer Key.
  -at  Twitter Consumer Secret.

  -ht  Hash tag to search.
  -s   <OPTIONAL> A duration "since" value formatted to YYYY-MM-DD. Defaults 24 hours.
`

// bindCredentials will use flag values over env vars.
func bindCredentials(as, at, ck, cs string) (*credentials, error) {
	c := &credentials{
		accessToken:       at,
		accessTokenSecret: as,
		consumerKey:       ck,
		consumerSecret:    cs,
	}

	if c.accessTokenSecret == "" {
		c.accessTokenSecret = os.Getenv("SWIT_ACCESS_TOKEN_SECRET")
	}
	if c.accessTokenSecret == "" {
		return c, errors.New("Missing required SWIT_ACCESS_TOKEN_SECRET")
	}

	if c.accessToken == "" {
		c.accessToken = os.Getenv("SWIT_ACCESS_TOKEN")
	}
	if c.accessToken == "" {
		return c, errors.New("Missing required SWIT_ACCESS_TOKEN")
	}

	if c.consumerKey == "" {
		c.consumerKey = os.Getenv("SWIT_CONSUMER_KEY")
	}
	if c.consumerKey == "" {
		return c, errors.New("Missing required SWIT_CONSUMER_KEY")
	}

	if c.consumerSecret == "" {
		c.consumerSecret = os.Getenv("SWIT_CONSUMER_SECRET")
	}
	if c.consumerSecret == "" {
		return c, errors.New("Missing required SWIT_CONSUMER_SECRET")
	}

	return c, nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, usage)
	}
	var as, at, ck, cs, ht string
	var since time.Duration
	flag.StringVar(&as, "as", "", "Access Secret")
	flag.StringVar(&at, "at", "", "Access Token")
	flag.StringVar(&ck, "ck", "", "Consumer Key")
	flag.StringVar(&cs, "cs", "", "Consumer Secret")
	flag.StringVar(&ht, "ht", "", "Hash Tag")
	flag.DurationVar(&since, "s", time.Hour*24, "Get results since ago")
	flag.Parse()

	creds, err := bindCredentials(as, at, ck, cs)
	if err != nil {
		errAndExit(err)
	}

	if ht == "" {
		errAndExit(errors.New("No Hash Tag value to search."))
	}

	config := &oauth1a.ClientConfig{
		ConsumerKey:    creds.consumerKey,
		ConsumerSecret: creds.consumerSecret,
	}
	user := oauth1a.NewAuthorizedConfig(creds.accessToken, creds.accessTokenSecret)
	client := twittergo.NewClient(config, user)

	query := url.Values{}
	query.Set("q", fmt.Sprintf("#%s", ht))
	query.Set("count", "100")
	query.Set("since", time.Now().UTC().Add(-1*since).Format("2006-01-02"))

	var next, url string
	endpoint := "/1.1/search/tweets.json"
	stop := false
	res := result{}
	for {
		if stop {
			break
		}

		if next == "" {
			url = fmt.Sprintf("%s?%v", endpoint, query.Encode())
		} else {
			url = fmt.Sprintf("%s%s", endpoint, next)
		}

		results, err := do(client, url)
		if err != nil {
			errAndExit(err)
		}

		res.count += len(results.Statuses())
		res.n++

		sm := results.SearchMetadata()
		if nr, ok := sm["next_results"]; ok {
			next = nr.(string)
		} else {
			next = ""
			stop = true
		}
	}

	fmt.Println(fmt.Sprintf("Made %d requests returning a total count of: %d", res.n, res.count))
}

func do(client *twittergo.Client, url string) (*twittergo.SearchResults, error) {
	fmt.Println("Making request with ", url)
	results := &twittergo.SearchResults{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return results, err
	}
	resp, err := client.SendRequest(req)
	if err != nil {
		return results, err
	}
	err = resp.Parse(results)
	if err != nil {
		return results, err
	}

	return results, err
}

// errAndExit displays any errors on ping execution along with flag usage.
func errAndExit(e error) {
	if e != nil {
		fmt.Fprint(os.Stderr, e)
		fmt.Fprintf(os.Stderr, "\n\n")
	}

	flag.Usage()
	fmt.Println()
	os.Exit(1)
}
