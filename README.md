# Swit - a simple Twitter Search

Created as a litle bit of fun to return a count of times a user provided hash tag appears on Twitter. By default this package will make requests for yesterday UTC.

```
Usage: swit [options...]

Options:
  -at  Twitter Access Token.
  -as  Twitter Access Token Secret.
  -ck  Twitter Consumer Key.
  -at  Twitter Consumer Secret.

  -ht  Hash tag to search.
  -s   <OPTIONAL> A duration "since" value formatted to YYYY-MM-DD. Defaults 24 hours.
```

This package will check for the environment vars of `SWIT_ACCESS_TOKEN`, `SWIT_ACCESS_TOKEN_SECRET`, `SWIT_CONSUMER_KEY`, `SWIT_CONSUMER_SECRET`. You can instead pass in these values which will override any env values set.

Use [https://apps.twitter.com](https://apps.twitter.com) to obtain the required values.
